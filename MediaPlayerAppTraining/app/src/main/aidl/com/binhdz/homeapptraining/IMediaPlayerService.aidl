package com.binhdz.homeapptraining;
import com.binhdz.homeapptraining.models.AppSettings;
import com.binhdz.homeapptraining.models.MusicItem;
import com.binhdz.homeapptraining.models.VideoItem;

interface IMediaPlayerService {
    AppSettings getCurrentAppSetting();
    void saveCurrentSongInfo(in MusicItem musicItem);
    void saveCurrentVideoInfo(in VideoItem videoItem);
}