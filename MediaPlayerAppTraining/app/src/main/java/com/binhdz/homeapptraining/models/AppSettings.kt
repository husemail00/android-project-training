package com.binhdz.homeapptraining.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
@Parcelize
data class AppSettings(
    var language: Int = 0,
    var volume: Int = 5
) : Parcelable