package com.binhdz.homeapptraining.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class VideoItem(
    val id: Int,
    val title: String,
    val artist: String,
    val path: String,
    val duration: Long
) :
    Parcelable