package com.binhdz.homeapptraining.models
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
@Parcelize
data class MusicItem(
    val id: Int,
    var title: String,
    var artist: String,
    var album: String,
    var duration: Int,
    val path: String
) :
    Parcelable