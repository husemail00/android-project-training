package com.binhdz.mediaplayerapptraining.utils
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.os.Environment
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.mediaplayerapptraining.R
import java.io.File
// get song image
fun getEmbeddedAlbumArt(filePath: String, context: Context): Bitmap {
    val file = File(filePath)
    if (file.exists() && file.isFile) {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(filePath)
        val picture = retriever.embeddedPicture
        retriever.release()
        if (picture != null) {
            val bitmapImage = BitmapFactory.decodeByteArray(picture, 0, picture.size)
            if (bitmapImage != null)
                return bitmapImage
        }
    }
    return BitmapFactory.decodeResource(context.resources, R.drawable.ic_music_background)
}
fun getListSongFromStorage(): List<MusicItem> {
    val songInfoList = mutableListOf<MusicItem>()
    if (isExternalStorageReadable()) {
        val directory = File(Environment.getExternalStorageDirectory().absolutePath + "/Download")
        if (directory.exists() && directory.isDirectory) {
            val files = directory.listFiles()
            var id = 0
            files?.forEach { file ->
                if (file.isFile && isAudioFile(file)) {
                    val musicItem = getSongInfo(file, id++)
                    songInfoList.add(musicItem)
                }
            }
        }
    }
    return songInfoList
}
fun getSongInfo(file: File, id: Int): MusicItem {
    val retriever = MediaMetadataRetriever()
    val path = file.absolutePath
    retriever.setDataSource(path)
    val title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE) ?: file.name
    val album =
        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM) ?: "Unknown album"
    val artist =
        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST) ?: "Unknown artis"
    val duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION) ?: "0"
    retriever.release()
    return MusicItem(id, title, artist, album, duration.toInt(), path)
}
fun getListVideoFromStorage(): List<VideoItem> {
    val listVideo = mutableListOf<VideoItem>()
    if (isExternalStorageReadable()) {
        val directory = File(Environment.getExternalStorageDirectory().absolutePath + "/Download")
        if (directory.exists() && directory.isDirectory) {
            val files = directory.listFiles()
            var id = 0
            files?.forEach { file ->
                if (file.isFile && isVideoFile(file)) {
                    val videoItem = getVideoInfo(file, id++)
                    listVideo.add(videoItem)
                }
            }
        }
    }
    return listVideo
}
fun getVideoInfo(file: File, id: Int): VideoItem {
    val retriever = MediaMetadataRetriever()
    val path = file.absolutePath
    retriever.setDataSource(path)
    val title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE) ?: file.name
    val artist =
        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST) ?: "Unknown artist"
    val duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION) ?: "0"
    retriever.release()
    return VideoItem(id, title, artist, path, duration.toLong())
}
fun isExternalStorageReadable(): Boolean {
    val state = Environment.getExternalStorageState()
    return Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state
}
fun isAudioFile(file: File): Boolean {
    val fileName = file.name.toLowerCase()
    return fileName.endsWith(".mp3") || fileName.endsWith(".wav") || fileName.endsWith(".ogg")
}
fun isVideoFile(file: File): Boolean {
    val supportedVideoExtensions = listOf(".mp4", ".3gp", ".mkv", ".flv")
    val fileName = file.name.toLowerCase()
    return supportedVideoExtensions.any { fileName.endsWith(it) }
}