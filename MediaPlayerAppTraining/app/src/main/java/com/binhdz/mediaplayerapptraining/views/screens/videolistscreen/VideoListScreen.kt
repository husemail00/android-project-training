package com.binhdz.mediaplayerapptraining.views.screens.videolistscreen
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.mediaplayerapptraining.utils.formatSongTime
import com.binhdz.mediaplayerapptraining.viewmodels.MediaViewModel
import com.binhdz.mediaplayerapptraining.views.appbar.MediaListAppBar
import com.binhdz.mediaplayerapptraining.views.screens.loadingscreen.LoadingScreen
@Composable
fun VideoListScreen(navController: NavController, mediaViewModel: MediaViewModel) {
    val videoList by mediaViewModel.videoListState.collectAsState(emptyList())
    Scaffold(
        topBar = {
            MediaListAppBar(navController)
        },
        content = { innerPadding ->
            if (videoList.isEmpty()) {
                LoadingScreen(innerPadding)
            } else {
                VideoListScreenContent(innerPadding, navController, mediaViewModel)
            }
        }
    )
}
@Composable
fun VideoListScreenContent(
    innerPadding: PaddingValues,
    navController: NavController,
    mediaViewModel: MediaViewModel
) {
    val videoList = mediaViewModel.getVideoList()
    val listState = rememberLazyListState()
    LazyColumn(
        state = listState,
        modifier = Modifier
            .fillMaxSize()
            .padding(innerPadding)
    ) {
        itemsIndexed(videoList) { index, videoItem ->
            VideoListItem(
                videoItem = videoItem,
                onPlayClick = {
                    navController.navigate("videoplayback/${videoItem.id}")
                })
            Divider(
                color = Color.Blue,
                thickness = 1.dp,
                modifier = Modifier.padding(start = 16.dp, end = 16.dp)
            )
        }
    }
}
@Composable
fun VideoListItem(
    videoItem: VideoItem,
    onPlayClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(80.dp)
            .background(
                Color.Transparent,
                shape = RoundedCornerShape(4.dp)
            )
            .clickable {
                onPlayClick()
            }
    ) {
        Row(
            Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            Text(
                text = videoItem.title,
                overflow = TextOverflow.Ellipsis,
                maxLines = 2,
                color = Color.Black,
                fontWeight = FontWeight.Normal,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp)
                    .weight(1f)
            )
            Text(
                text = formatSongTime(videoItem.duration.toFloat()),
                modifier = Modifier.padding(end = 16.dp)
            )
        }
    }
}