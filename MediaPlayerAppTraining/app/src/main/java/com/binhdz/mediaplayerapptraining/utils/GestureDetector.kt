package com.binhdz.mediaplayerapptraining.utils

import android.util.Log
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput

@Composable
fun GestureDetector(
    modifier: Modifier = Modifier.fillMaxSize(),
    doWhenSwipeLeft: () -> Unit,
    doWhenSwipeRight: () -> Unit
) {
    Box(
        modifier
            .pointerInput(Unit) {
                detectTransformGestures { _, panGesture, _, _ ->
                    when {
                        panGesture.x > 30 -> doWhenSwipeRight()
                        panGesture.x < 0 -> doWhenSwipeLeft()
                        else -> "No change!"
                    }
                }
            }
    ) {
// noi dung vung man hinh
    }
}