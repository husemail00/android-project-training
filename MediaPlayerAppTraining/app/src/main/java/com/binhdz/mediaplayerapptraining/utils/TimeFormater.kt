package com.binhdz.mediaplayerapptraining.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun formatSongTime(millis: Float): String {
    val seconds = (millis / 1000).toInt()
    val minutes = seconds / 60
    val remainingSeconds = seconds % 60
    return String.format("%02d:%02d", minutes, remainingSeconds)
}

fun formatAppbarTime(time: LocalDateTime): String {
    val formatter = DateTimeFormatter.ofPattern("hh:mm a dd.M")
    return time.format(formatter)
}