package com.binhdz.mediaplayerapptraining.views.appbar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.binhdz.mediaplayerapptraining.utils.formatAppbarTime
import kotlinx.coroutines.delay
import java.time.LocalDateTime
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MusicPlayBackAppBar( navController: NavController) {
    TopAppBar(
        colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.Black),
        title = {
            var currentTime by remember { mutableStateOf(LocalDateTime.now()) }
            LaunchedEffect(Unit) {
                while (true) {
                    delay(1000)
                    currentTime = LocalDateTime.now()
                }
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .background(color = Color.Black),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = formatAppbarTime(currentTime),
                    color = Color.White,
                    style = TextStyle(
                        color = Color.White,
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        },
        navigationIcon = {
            Row {
                IconButton(onClick = {
                }) {
                    Icon(Icons.Default.ArrowBack, contentDescription = null, tint = Color.White)
                }
                IconButton(onClick = { /* back to home */ }) {
                    Icon(Icons.Default.Home, contentDescription = null, tint = Color.White)
                }
                IconButton(onClick = {
                    navController.navigate("musiclist")
                }) {
                    Icon(Icons.Default.List, contentDescription = null, tint = Color.White)
                }
            }
        },
    )
}