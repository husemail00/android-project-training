package com.binhdz.mediaplayerapptraining.viewmodels
import android.media.MediaPlayer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.mediaplayerapptraining.utils.getListSongFromStorage
import com.binhdz.mediaplayerapptraining.utils.getListVideoFromStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
class MediaViewModel : ViewModel() {
    private val _mediaPlayerState = MutableStateFlow(MediaPlayer())
    private val _musicListState = MutableStateFlow<List<MusicItem>>(emptyList())
    private val _videoListState = MutableStateFlow<List<VideoItem>>(emptyList())
    private val _currentSongIndexState = MutableStateFlow(-1)
    private val _currentVideoIndexState = MutableStateFlow(-1)
    private val _isPlayingState = MutableStateFlow(true)
    val mediaPlayerState: StateFlow<MediaPlayer> get() = _mediaPlayerState
    val musicListState: StateFlow<List<MusicItem>> get() = _musicListState
    val videoListState: StateFlow<List<VideoItem>> get() = _videoListState
    val currentSongIndex: StateFlow<Int>
        get() = _currentSongIndexState
    val currentVideoIndex: StateFlow<Int>
        get() = _currentVideoIndexState
    fun currentSongState(): Flow<MusicItem> = currentSongIndex.map { _musicListState.value[it] }
    init {
        viewModelScope.launch(Dispatchers.IO) {
            setMusicList(getListSongFromStorage())
        }
        viewModelScope.launch(Dispatchers.IO) {
            setVideoList(getListVideoFromStorage())
        }
    }
    fun getMediaPlayer(): MediaPlayer = _mediaPlayerState.value
    fun getMusicList(): List<MusicItem> = _musicListState.value
    fun getVideoList(): List<VideoItem> = _videoListState.value
    private fun getSongAt(index: Int): MusicItem = _musicListState.value[index]
    private fun setMusicList(musicList: List<MusicItem>) {
        _musicListState.value = musicList
    }
    fun getVideoItemById(videoId: Int): VideoItem? {
        return _videoListState.value.find { it.id == videoId }
    }

    private fun setVideoList(videoList: List<VideoItem>) {
        _videoListState.value = videoList
    }
    fun setCurrentSong(index: Int) {
        _currentSongIndexState.value = index
    }
    fun playSongAt(index: Int) {
        _currentSongIndexState.value = index % _musicListState.value.size
        val song: MusicItem = getSongAt(_currentSongIndexState.value)
        _mediaPlayerState.value.reset()
        _mediaPlayerState.value.setDataSource(song.path)
        _mediaPlayerState.value.prepare()
        _mediaPlayerState.value.start()
// Xử lý khi phát nhạc xong thì next bài
        _mediaPlayerState.value.setOnCompletionListener {
            _currentSongIndexState.value =
                (_currentSongIndexState.value + 1) % _musicListState.value.size
            _mediaPlayerState.value.reset()
            _mediaPlayerState.value.setDataSource(_musicListState.value[_currentSongIndexState.value].path)
            _mediaPlayerState.value.prepare()
            _mediaPlayerState.value.start()
        }
    }

    fun playOrPauseMedia() {
        if (!_mediaPlayerState.value.isPlaying) {
            _mediaPlayerState.value.start()
        } else {
            _mediaPlayerState.value.pause()
        }
    }
    fun playNextSong() {
        playSongAt(_currentSongIndexState.value + 1)
    }
    fun playPreviousSong() {
        if (_mediaPlayerState.value.currentPosition < 3000 && _currentSongIndexState.value > 0) {
            playSongAt(_currentSongIndexState.value - 1)
        } else {
            playSongAt(_currentSongIndexState.value)
        }
    }
    fun playVideoAt(index: Int) {
        val safeIndex = index % _videoListState.value.size
        _currentVideoIndexState.value = safeIndex
        val videoItem = _videoListState.value[safeIndex]
        _mediaPlayerState.value.apply {
            reset()
            setDataSource(videoItem.path)
            prepare()
            start()
            setOnCompletionListener {
                playNextVideo()
            }
        }
    }
    fun setCurrentVideo(index: Int) {
        _currentVideoIndexState.value = index
    }
    val currentVideoPath: Flow<String?> = combine( // hàm này bị sai
        videoListState,
        currentVideoIndex
    ) { videoList, currentIndex ->
        if (currentIndex in videoList.indices) {
            videoList[currentIndex].path
        } else {
            null
        }
    }
        .distinctUntilChanged()

    val currentVideoItem: Flow<VideoItem> = combine( // lấy ra current video
        videoListState,
        currentVideoIndex
    ) { videoList, currentIndex ->
        videoList[currentIndex]
    }
    fun playOrPauseVideo() {
        with(_mediaPlayerState.value) {
            if (isPlaying) {
                pause()
                _isPlayingState.value = false
            } else {
                start()
                _isPlayingState.value = true
            }
        }
    }

    fun playNextVideo() {
// đã có thể next được VideoItem nhưng màn hình vẫn play video cũ
        _currentVideoIndexState.value = (_currentVideoIndexState.value +1) % _videoListState.value.size
    }

    fun playPreviousVideo() {
        val prevIndex = if (_currentVideoIndexState.value - 1 < 0) {
            _videoListState.value.size - 1
        } else {
            _currentVideoIndexState.value - 1
        }
        playVideoAt(prevIndex)
    }
    fun seekTo(position: Int) {
        _mediaPlayerState.value.seekTo(position)
    }
    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
    }
    private fun releaseMediaPlayer() {
        getMediaPlayer().release()
    }
}