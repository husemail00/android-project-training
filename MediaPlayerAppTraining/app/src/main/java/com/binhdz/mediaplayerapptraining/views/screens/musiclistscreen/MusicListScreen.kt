package com.binhdz.mediaplayerapptraining.views.screens.musiclistscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.mediaplayerapptraining.utils.GifImage
import com.binhdz.mediaplayerapptraining.utils.formatSongTime
import com.binhdz.mediaplayerapptraining.viewmodels.MediaViewModel
import com.binhdz.mediaplayerapptraining.views.appbar.MediaListAppBar
import com.binhdz.mediaplayerapptraining.views.screens.loadingscreen.LoadingScreen
import kotlinx.coroutines.delay

@Composable
fun MusicListScreen(navController: NavController, mediaViewModel: MediaViewModel) {
    val musicList by mediaViewModel.musicListState.collectAsState(emptyList())
    Scaffold(
        topBar = {
            MediaListAppBar(navController)
        },
        content = { innerPadding ->
            if (musicList.isEmpty()) {
                LoadingScreen(innerPadding)
            } else {
                MusicListScreenContent(innerPadding, mediaViewModel)
            }
        }
    )
}

@Composable
fun MusicListScreenContent(
    innerPadding: PaddingValues,
    mediaViewModel: MediaViewModel
) {
    val musicList = mediaViewModel.getMusicList()
    val listState = rememberLazyListState()
    val currentPlayingItem by mediaViewModel.currentSongState().collectAsState(
        MusicItem(
            -1,
            "Unknown Song",
            "Unknown Artis",
            "Unknown Album",
            0,
            "Unknown Path"
        )
    )
    LazyColumn(
        state = listState,
        modifier = Modifier
            .fillMaxSize()
            .padding(innerPadding)
    ) {
        itemsIndexed(musicList) { index, musicItem ->
            MusicListItem(
                musicItem = musicItem,
                isPlaying = musicItem.id == currentPlayingItem.id,
                onPlayClick = {
                    mediaViewModel.playSongAt(index)
                })
            Divider(
                color = Color.Blue,
                thickness = 1.dp,
                modifier = Modifier.padding(start = 16.dp, end = 16.dp)
            )
        }
    }
    LaunchedEffect(Unit) {
        delay(500)
        listState.animateScrollToItem(if (currentPlayingItem.id - 1 > 0) (currentPlayingItem.id - 1) else currentPlayingItem.id)
    }
}

@Composable
fun MusicListItem(
    musicItem: MusicItem,
    isPlaying: Boolean,
    onPlayClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(80.dp)
            .background(
                Color.Transparent,
                shape = RoundedCornerShape(4.dp)
            )
            .clickable {
                onPlayClick()
            }
    ) {
        Row(
            Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            if (isPlaying)
                GifImage(
                    modifier = Modifier
                        .height(40.dp)
                        .width(55.dp)
                        .padding(start = 16.dp)
                )
            else Spacer(modifier = Modifier)
            Text(
                text = musicItem.title,
                overflow = TextOverflow.Ellipsis,
                maxLines = 2,
                color = if (isPlaying) (Color.Red) else (Color.Black),
                fontWeight = if (isPlaying) FontWeight.Bold else FontWeight.Normal,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp)
                    .weight(1f)
            )
            Text(
                text = formatSongTime(musicItem.duration.toFloat()),
                modifier = Modifier.padding(end = 16.dp)
            )
        }
    }
}
