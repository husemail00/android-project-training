package com.binhdz.mediaplayerapptraining.views.screens.videoplaybackscreen
import android.app.Activity
import android.media.MediaPlayer
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.FrameLayout
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.SkipNext
import androidx.compose.material.icons.filled.SkipPrevious
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.NavController
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.mediaplayerapptraining.utils.formatSongTime
import com.binhdz.mediaplayerapptraining.viewmodels.MediaViewModel
import com.binhdz.mediaplayerapptraining.views.appbar.VideoPlayBackAppBar
import kotlinx.coroutines.delay

@Composable
fun VideoPlaybackScreen(
    navController: NavController,
    mediaViewModel: MediaViewModel
) {
    var isFullScreen by remember { mutableStateOf(false) }
    fun toggleFullScreen() {
        isFullScreen = !isFullScreen
    }
    Scaffold(
        topBar = {
            if (!isFullScreen)
                VideoPlayBackAppBar(navController, ::toggleFullScreen)
        },
        content = {
            VideoPlaybackScreenContent(
                it,
                mediaViewModel,
                isFullScreen,
                ::toggleFullScreen
            )
        }
    )
}

@Composable
fun VideoPlaybackScreenContent(
    paddingValues: PaddingValues,
    mediaViewModel: MediaViewModel,
    isFullScreen: Boolean,
    toggleFullScreen: () -> Unit
) {
    val activity = LocalContext.current as Activity
    val isPlaying = remember { mutableStateOf(true) }
    val controlsVisible = remember { mutableStateOf(true) } // hiển thị playback controls

    Box(modifier = Modifier.fillMaxSize()) {
// màn hình có 3 lớp theo thứ tự: lớp video - lớp chạm - lớp playback control
// phần I: hiển thị video
        VideoPlayerComposable(mediaViewModel)
// phần II: detect 1 chạm ẩn hiện Playback controls, phần này ok
        Box(modifier = Modifier
            .matchParentSize()
            .clickable {
                controlsVisible.value = !controlsVisible.value
                if (controlsVisible.value) {
                    toggleFullScreen()
                    showSystemUI(activity)
                } else {
                    hideSystemUI(activity)
                }
            }
        )
// phần III: hiển thị Playback controls
        AnimatedVisibility(visible = controlsVisible.value && !isFullScreen) { // cần thay đổi logic hiển thị
            VideoControlComposable(paddingValues, mediaViewModel)
        }
    }
// tự động ẩn các nút và bật full màn hình sau 10s, ok
    LaunchedEffect(isPlaying.value) {
        delay(10000)
        controlsVisible.value = false
        toggleFullScreen()
        hideSystemUI(activity)
    }
}

// phần I: hiển thị video
@Composable
fun VideoPlayerComposable(
    mediaViewModel: MediaViewModel
) {
    val mediaPlayer = mediaViewModel.getMediaPlayer()
    val videoPath by mediaViewModel.currentVideoPath.collectAsState(null)
    if (videoPath != null) {
        AndroidView(factory = { context ->
            SurfaceView(context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
                holder.addCallback(object : SurfaceHolder.Callback {
                    override fun surfaceCreated(holder: SurfaceHolder) {
                        mediaPlayer.setDataSource(videoPath)
                        mediaPlayer.setDisplay(holder)
                        mediaPlayer.prepare()
                        mediaPlayer.start()
                    }
                    override fun surfaceChanged(
                        holder: SurfaceHolder,
                        format: Int,
                        width: Int,
                        height: Int
                    ) {
// Xử lý thay đổi bề mặt nếu cần (sự thay đổi về format, width hoặc height)
                    }
                    override fun surfaceDestroyed(holder: SurfaceHolder) {
                        mediaPlayer.reset()
                    }
                })
            }
        }, modifier = Modifier.fillMaxSize())
    }

}

////////////////////////// ok ///////////////////////
fun hideSystemUI(activity: Activity) { // Ẩn cả thanh điều hướng và thanh app bar
    val windowInsetsController =
        WindowInsetsControllerCompat(activity.window, activity.window.decorView)
    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
}
fun showSystemUI(activity: Activity) { // Hiển thị lại thanh điều hướng và thanh app bar
    val windowInsetsController =
        WindowInsetsControllerCompat(activity.window, activity.window.decorView)
    windowInsetsController.show(WindowInsetsCompat.Type.systemBars())
}

/////////////////////////////// phần II: hiển thị Playback controls
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun VideoControlComposable(
    paddingValues: PaddingValues,
    mediaViewModel: MediaViewModel
) {
    var isMediaPlaying by remember { mutableStateOf(false) }
    var currentPlayBackPosition by remember { mutableFloatStateOf(0f) }
    val mediaPlayer = mediaViewModel.getMediaPlayer()
    val currentVideoID by mediaViewModel.currentVideoIndex.collectAsState()

// cập nhật playbackPosition
    LaunchedEffect(mediaPlayer) {
        while (true) {
            if (mediaPlayer.isPlaying) {
                currentPlayBackPosition = mediaPlayer.currentPosition.toFloat()
            }
            isMediaPlaying = mediaPlayer.isPlaying
            delay(200)
        }
    }
//////////////////////////// UI ////////////////
    ConstraintLayout(
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
    ) {
        val (songtitle, rowbuttons, rowtimeinfor) = createRefs()
// row hiển thị control buttons
        Row(
            modifier = Modifier
                .constrainAs(rowbuttons) {
                    centerHorizontallyTo(parent)
                    centerVerticallyTo(parent)
                },
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.weight(1f))
            IconButton(
                onClick = {
                }) {
                Icon(
                    Icons.Default.SkipPrevious,
                    contentDescription = "Previous",
                    Modifier
                        .fillMaxSize()
                        .background(color = Color.White)
                )
            }
            Spacer(modifier = Modifier.width(40.dp))
            IconButton(
                onClick = {
                    mediaViewModel.playOrPauseVideo()
                }) {
                Icon(
                    if (isMediaPlaying) Icons.Default.Pause else Icons.Default.PlayArrow,
                    contentDescription = "Play",
                    Modifier
                        .fillMaxSize()
                        .background(color = Color.White)
                )
            }
            Spacer(modifier = Modifier.width(40.dp))
            IconButton(
                onClick = {
// mediaViewModel.playNextVideo() // tạm thời không sử dụng chức năng này
                }) {
                Icon(
                    Icons.Default.SkipNext,
                    contentDescription = "Next",
                    Modifier
                        .fillMaxSize()
                        .background(color = Color.White)
                )
            }
            Spacer(modifier = Modifier.weight(1f))
        }
// video name
        Text(
            mediaViewModel.getVideoList()[currentVideoID].title,
            modifier = Modifier
                .basicMarquee(
                    Int.MAX_VALUE,
                    animationMode = MarqueeAnimationMode.Immediately,
                    delayMillis = 1000
                )
                .padding(start = 40.dp)
                .constrainAs(songtitle) {
                    bottom.linkTo(rowtimeinfor.top)
                },
            style = TextStyle(
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 28.sp,
            )
        )
// slider
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 40.dp, end = 40.dp, bottom = 40.dp)
                .constrainAs(rowtimeinfor) {
                    bottom.linkTo(parent.bottom)
                },
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                formatSongTime(currentPlayBackPosition),
                style = TextStyle(
                    color = Color.White,
                    fontSize = 25.sp,
                )
            )
            Slider(
                value = currentPlayBackPosition,
                onValueChange = { newPosition ->
                    currentPlayBackPosition = newPosition
                    mediaPlayer.seekTo(newPosition.toInt())
                },
                valueRange = 0f..mediaPlayer.duration.toFloat(),
                modifier = Modifier
                    .height(30.dp)
                    .padding(start = 16.dp, end = 16.dp)
                    .weight(1f),
                interactionSource = remember { MutableInteractionSource() }
            )
            Text(
                formatSongTime(mediaPlayer.duration.toFloat()),
                style = TextStyle(
                    color = Color.White,
                    fontSize = 25.sp,
                )
            )
        }
    }
}

