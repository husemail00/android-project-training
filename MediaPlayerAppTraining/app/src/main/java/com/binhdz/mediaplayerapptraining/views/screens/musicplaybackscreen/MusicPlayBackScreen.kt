package com.binhdz.mediaplayerapptraining.views.screens.musicplaybackscreen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.SkipNext
import androidx.compose.material.icons.filled.SkipPrevious
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.mediaplayerapptraining.utils.GestureDetector
import com.binhdz.mediaplayerapptraining.utils.formatSongTime
import com.binhdz.mediaplayerapptraining.utils.getEmbeddedAlbumArt
import com.binhdz.mediaplayerapptraining.viewmodels.MediaViewModel
import com.binhdz.mediaplayerapptraining.views.appbar.MusicPlayBackAppBar
import com.binhdz.mediaplayerapptraining.views.screens.loadingscreen.LoadingScreen
import kotlinx.coroutines.delay

@Composable
fun MusicPlayBackScreen(
    navController: NavController,
    mediaViewModel: MediaViewModel
) {
    val musicList by mediaViewModel.musicListState.collectAsState(emptyList())
    Scaffold(
        topBar = {
            MusicPlayBackAppBar(navController)
        },
        content = { innerPadding ->
            if (musicList.isEmpty()) {
                LoadingScreen(innerPadding)
            } else {
                MusicPlayBackScreenContent(innerPadding, mediaViewModel)
            }
        }
    )
}
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MusicPlayBackScreenContent(
    innerPadding: PaddingValues,
    mediaViewModel: MediaViewModel
) {
    val context = LocalContext.current
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp
    val screenHeight = configuration.screenHeightDp
    val imageSize = (screenHeight / 1.5).dp
    val rowButtonsWidth = (screenWidth / 2).dp
    val rowButtonsHeight = (screenHeight / 8).dp
    val columnSongDetailWidth = (screenWidth / 2).dp
    val columnSongDetailHeight = (screenHeight / 2.5).dp
    val currentSongID = mediaViewModel.currentSongIndex.collectAsState()
    var currentPlayBackPosition by remember { mutableFloatStateOf(0f) }
    var isMediaPlaying by remember { mutableStateOf(false) }
    val mediaPlayer by mediaViewModel.mediaPlayerState.collectAsState()
    val currentSong by mediaViewModel.currentSongState().collectAsState(
        MusicItem(
            -1,
            "Unknown Song",
            "Unknown Artis",
            "Unknown Album",
            0,
            "Unknown Path"
        )
    )
    var lastSwipeTime = 0L
    val currentSongBitmapImage by rememberUpdatedState(
        getEmbeddedAlbumArt(
            currentSong.path,
            context
        )
    )
// tự động play bài nhạc hiện tại khi mở màn hoặc play bài đầu tiên khi mở lần đầu
    if (!mediaViewModel.getMediaPlayer().isPlaying) {
        if (currentSongID.value == -1)
            mediaViewModel.playSongAt(0)
        else
            mediaViewModel.playSongAt(currentSongID.value)
    }
// cập nhật playbackPosition
    LaunchedEffect(mediaPlayer) {
        while (true) {
            if (mediaPlayer.isPlaying) {
                currentPlayBackPosition = mediaPlayer.currentPosition.toFloat()
            }
            isMediaPlaying = mediaPlayer.isPlaying
            delay(200)
        }
    }
//////////////////////////// UI ////////////////
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(innerPadding)
    ) {
        GestureDetector(
            doWhenSwipeLeft = {
                val currentTime = System.currentTimeMillis()
                if (currentTime - lastSwipeTime > 500) {
                    mediaViewModel.playNextSong()
                    lastSwipeTime = currentTime
                }
            },
            doWhenSwipeRight = {
                val currentTime = System.currentTimeMillis()
                if (currentTime - lastSwipeTime > 500) {
                    mediaViewModel.playPreviousSong()
                    lastSwipeTime = currentTime
                }
            })
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val (columnsongdetail, rowbuttons, rowtimeinfor, image) = createRefs()
            Image(
                painter = rememberAsyncImagePainter(currentSongBitmapImage),
                contentDescription = "Song Image",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(imageSize)
                    .padding(20.dp)
                    .clip(shape = MaterialTheme.shapes.medium)
                    .constrainAs(image) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }
            )
            Column(verticalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier
                    .height(columnSongDetailHeight)
                    .width(columnSongDetailWidth)
                    .constrainAs(columnsongdetail) {
                        start.linkTo(parent.start, margin = 30.dp)
                        top.linkTo(parent.top)
                    }) {
                Text(
                    currentSong.title, maxLines = 1,
                    style = TextStyle(
                        fontSize = 30.sp,
                        fontWeight = FontWeight.Bold
                    ),
                    modifier = Modifier.basicMarquee(
                        Int.MAX_VALUE,
                        animationMode = MarqueeAnimationMode.Immediately,
                        delayMillis = 1000
                    )
                )
                Text(
                    currentSong.artist, maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = TextStyle(
                        fontSize = 25.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
                Text(
                    currentSong.album, maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = TextStyle(
                        fontSize = 25.sp,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(40.dp)
                    .constrainAs(rowtimeinfor) {
                        bottom.linkTo(parent.bottom)
                    },
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    formatSongTime(currentPlayBackPosition),
                    style = TextStyle(
                        fontSize = 28.sp,
                    )
                )
                Slider(
                    value = currentPlayBackPosition,
                    onValueChange = { newPosition ->
                        currentPlayBackPosition = newPosition
                        mediaPlayer.seekTo(newPosition.toInt())
                    },
                    valueRange = 0f..mediaPlayer.duration.toFloat(),
                    modifier = Modifier
                        .height(30.dp)
                        .padding(start = 16.dp, end = 16.dp)
                        .weight(1f),
                    interactionSource = remember { MutableInteractionSource() }
                )
                Text(
                    formatSongTime(mediaPlayer.duration.toFloat()),
                    style = TextStyle(
                        fontSize = 28.sp,
                    )
                )
            }
            Row(
                modifier = Modifier
                    .width(rowButtonsWidth)
                    .height(rowButtonsHeight)
                    .constrainAs(rowbuttons) {
                        start.linkTo(parent.start, margin = 32.dp)
                        top.linkTo(columnsongdetail.bottom)
                        bottom.linkTo(image.bottom)
                    },
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    modifier = Modifier
                        .weight(1f)
                        .size(rowButtonsHeight),
                    onClick = {
                        mediaViewModel.playPreviousSong()
                    }) {
                    Icon(
                        Icons.Default.SkipPrevious,
                        contentDescription = "Previous",
                        Modifier.fillMaxSize()
                    )
                }
                Spacer(modifier = Modifier.width(16.dp))
                IconButton(
                    modifier = Modifier
                        .weight(1f)
                        .size(rowButtonsHeight),
                    onClick = {
                        mediaViewModel.playOrPauseMedia()
                    }) {
                    Icon(
                        if (isMediaPlaying) Icons.Default.Pause else Icons.Default.PlayArrow,
                        contentDescription = "Play",
                        Modifier.fillMaxSize()
                    )
                }
                Spacer(modifier = Modifier.width(16.dp))
                IconButton(
                    modifier = Modifier
                        .weight(1f)
                        .size(rowButtonsHeight),
                    onClick = {
                        mediaViewModel.playNextSong()
                    }) {
                    Icon(
                        Icons.Default.SkipNext,
                        contentDescription = "Next",
                        Modifier.fillMaxSize()
                    )
                }
            }
        }
    }
}