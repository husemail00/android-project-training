package com.binhdz.mediaplayerapptraining

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.binhdz.mediaplayerapptraining.views.screens.musiclistscreen.MusicListScreen
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import android.widget.Toast
import androidx.compose.foundation.layout.PaddingValues
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavType
import androidx.navigation.navArgument
import androidx.navigation.navDeepLink
import kotlinx.coroutines.launch
import android.media.AudioManager
import android.util.Log
import androidx.activity.viewModels
import androidx.compose.runtime.collectAsState
import com.binhdz.homeapptraining.IMediaPlayerService
import com.binhdz.homeapptraining.models.AppSettings
import com.binhdz.mediaplayerapptraining.animations.enterTransition
import com.binhdz.mediaplayerapptraining.animations.exitTransition
import com.binhdz.mediaplayerapptraining.viewmodels.MediaViewModel
import com.binhdz.mediaplayerapptraining.views.screens.loadingscreen.LoadingScreen
import com.binhdz.mediaplayerapptraining.views.screens.musicplaybackscreen.MusicPlayBackScreen
import com.binhdz.mediaplayerapptraining.views.screens.videolistscreen.VideoListScreen
import com.binhdz.mediaplayerapptraining.views.screens.videoplaybackscreen.VideoPlaybackScreen
import kotlinx.coroutines.delay

class MainActivity : AppCompatActivity() {
    private lateinit var mediaPlayerService: IMediaPlayerService
    private lateinit var appSettings: AppSettings
    private val mediaViewModel: MediaViewModel by viewModels()
    private lateinit var audioManager: AudioManager
    private var isMediaPlayerServiceConnected = false

    // Bind service
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            isMediaPlayerServiceConnected = true
            mediaPlayerService = IMediaPlayerService.Stub.asInterface(service)
            appSettings = mediaPlayerService.currentAppSetting
            adjustMediaPlayerVolume()
            checkPermissions()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            isMediaPlayerServiceConnected = false
            setContent {
                LoadingScreen(PaddingValues(16.dp), 1)
            }
            tryReconnectService()
        }
    }

    fun tryReconnectService() {
        if (!isMediaPlayerServiceConnected) {
            mediaViewModel.viewModelScope.launch {
                delay(2000) // Sau 2 giây tự động kết nối lại
                bindMediaPlayerService(serviceConnection)
            }
        }
    }

    override fun onStop() {
        mediaViewModel.viewModelScope.launch {
            mediaViewModel.currentSongIndex.collect { index ->
                if (index != -1)
                    mediaPlayerService.saveCurrentSongInfo(mediaViewModel.getMusicList()[index])
            }
        }
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        mediaViewModel.viewModelScope.launch {
            mediaViewModel.currentVideoIndex.collect { index ->
                if (index != -1) {
                    val videoItem = mediaViewModel.getVideoList()[index]
                    mediaPlayerService.saveCurrentVideoInfo(videoItem)
                    Log.d("check123", "Media: thực hiện gửi video: ${videoItem}")
                }
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
// đặt lại volume mỗi khi quay lại app
        if (isMediaPlayerServiceConnected)
            appSettings = mediaPlayerService.currentAppSetting
        adjustMediaPlayerVolume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        audioManager = applicationContext.getSystemService(AUDIO_SERVICE) as AudioManager
        bindMediaPlayerService(serviceConnection)
    }

    private fun bindMediaPlayerService(serviceConnection: ServiceConnection) {
        val serviceIntent = Intent("com.binhdz.homeapptraining.MediaPlayerService")
        serviceIntent.`package` = "com.binhdz.homeapptraining"
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun checkPermissions() {
        Dexter.withContext(this)
            .withPermissions(
                READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report?.areAllPermissionsGranted() == true) {
                        setContent {
                            TrainingApp()
                        }
                    } else {
                        Toast.makeText(this@MainActivity, "Permission denied!", Toast.LENGTH_LONG)
                            .show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .check()
    }

    fun adjustMediaPlayerVolume() {
        val volumeLevel = appSettings.volume.toFloat() / 10
        mediaViewModel.getMediaPlayer().apply {
            setVolume(volumeLevel, volumeLevel)
        }
    }

    @Composable
    fun TrainingApp() {
        val navController = rememberNavController()
        val currentSongId = mediaViewModel.currentSongIndex.collectAsState()
        NavHost(
            navController = navController,
            startDestination = "videolist"
        ) {// cần để mặc định là musicplayback, hiện tại đang test videolist hoặc testscreen
            composable("musicplayback",
                enterTransition = { enterTransition() },
                exitTransition = { exitTransition() },
                deepLinks = listOf(
                    navDeepLink {
                        uriPattern =
                            "mediaplayerapptraining://com.binhdz.mediaplayerapptraining.musicplayback/{songID}"
                    },
                ),
                arguments = listOf(
                    navArgument("songID") {
                        type = NavType.IntType
                        defaultValue = -1
                    }
                )) {
                val songID = it.arguments?.getInt("songID") ?: -1
                if (songID == -1 && currentSongId.value == -1) {
                    mediaViewModel.setCurrentSong(0)
                } else if (songID != -1 && currentSongId.value == -1) {
                    mediaViewModel.setCurrentSong(songID)
                }
                MusicPlayBackScreen(navController, mediaViewModel)
            }
            composable(
                "videoplayback/{videoId}",
                arguments = listOf(navArgument("videoId") { type = NavType.IntType })
            ) {
                val videoId = it.arguments?.getInt("videoId") ?: -1
                mediaViewModel.setCurrentVideo(videoId)
                VideoPlaybackScreen(navController, mediaViewModel)
            }
            composable("musiclist",
                enterTransition = { enterTransition() },
                exitTransition = { exitTransition() }) {
                MusicListScreen(
                    navController,
                    mediaViewModel
                )
            }
            composable("videolist",
                deepLinks = listOf(
                    navDeepLink {
                        uriPattern =
                            "mediaplayerapptraining://com.binhdz.mediaplayerapptraining.videoplayback"
                    },
                ),
                enterTransition = { enterTransition() },
                exitTransition = { exitTransition() }) {
                VideoListScreen(
                    navController,
                    mediaViewModel
                )
            }
        }
    }
}