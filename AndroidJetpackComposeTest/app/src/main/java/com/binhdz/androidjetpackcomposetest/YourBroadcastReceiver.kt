package com.binhdz.androidjetpackcomposetest

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager

class YourBroadcastReceiver(private val updateUI: () -> Unit) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            WifiManager.WIFI_STATE_CHANGED_ACTION,
            BluetoothAdapter.ACTION_STATE_CHANGED,
            Intent.ACTION_AIRPLANE_MODE_CHANGED -> {
                updateUI()
            }
        }
    }
}
