package com.binhdz.androidjetpackcomposetest

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf

class MainActivity : ComponentActivity() {
    private lateinit var receiver: YourBroadcastReceiver
    private val wifiState = mutableStateOf(false) // false: tắt, true: bật
    private val bluetoothState = mutableStateOf(false)
    private val airplaneModeState = mutableStateOf(false)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val updateUI: () -> Unit = { /* Gọi lại Composable Function để cập nhật UI */
            wifiState.value = isWifiEnabled()
            bluetoothState.value = isBluetoothEnabled()
            airplaneModeState.value = isAirplaneModeOn()
        }
        receiver = YourBroadcastReceiver(updateUI)

        IntentFilter().apply {
            addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
            addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
            addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
            // Đăng ký thêm các action khác nếu cần
        }.also { filter ->
            registerReceiver(receiver, filter)
        }

        setContent {
            YourApp(wifiState.value, bluetoothState.value, airplaneModeState.value)
        }
    }

    // Các hàm kiểm tra trạng thái thiết bị
    private fun isWifiEnabled(): Boolean {
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return wifiManager.isWifiEnabled
    }

    private fun isBluetoothEnabled(): Boolean {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter?.isEnabled ?: false
    }

    private fun isAirplaneModeOn(): Boolean {
        return Settings.System.getInt(contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0) != 0
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }
}

@Composable
fun YourApp(wifiState: Boolean, bluetoothState: Boolean, airplaneModeState: Boolean) {
    Column {
        Text(text = if (wifiState) "WiFi: On" else "WiFi: Off")

        Text(text = if (bluetoothState) "Bluetooth: On" else "Bluetooth: Off")

        Text(text = if (airplaneModeState) "Airplane Mode: On" else "Airplane Mode: Off")
    }
}
