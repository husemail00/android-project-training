package com.binhdz.homeapptraining.views.screens.settingscreen.settingoptions

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.binhdz.homeapptraining.viewmodels.MediaViewModel

@Composable
fun AdjustVolume(mediaViewModel: MediaViewModel) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(end = 150.dp)
    ) {
        SliderAdvancedExample( mediaViewModel)
    }
}

@Composable
fun SliderAdvancedExample( musicViewModel: MediaViewModel) {
    val appSettings by musicViewModel.appSettingsState.collectAsState()
    var sliderPosition by remember { mutableStateOf(appSettings.volume.toFloat()) }

    fun saveSelection(newVolume: Int) {
        val updatedSettings = appSettings.copy(volume = newVolume)
        musicViewModel.updateAppSettings(updatedSettings)
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(start = 100.dp, end = 100.dp)
    ) {
        Text(
            text = sliderPosition.toInt().toString(),
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            )
        )
        Spacer(modifier = Modifier.height(32.dp))
        Slider(
            value = sliderPosition,
            onValueChange = {
                sliderPosition = it
                saveSelection(it.toInt())
            },
            colors = SliderDefaults.colors(
                thumbColor = Color.Blue,
                activeTrackColor = Color.Blue.copy(alpha = 0.3f),
                inactiveTrackColor = MaterialTheme.colorScheme.secondaryContainer,
            ),
            steps = 9,
            valueRange = 0f..10f
        )
    }
}