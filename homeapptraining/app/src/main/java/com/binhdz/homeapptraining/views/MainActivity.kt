package com.binhdz.homeapptraining.views

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.binhdz.homeapptraining.animations.enterTransition
import com.binhdz.homeapptraining.animations.exitTransition
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.homeapptraining.services.MediaPlayerService
import com.binhdz.homeapptraining.utils.getMusicItemFromPreferences
import com.binhdz.homeapptraining.utils.saveMusicItemToPreferences
import com.binhdz.homeapptraining.utils.saveVideoItemToPreferences
import com.binhdz.homeapptraining.viewmodels.MediaViewModel
import com.binhdz.homeapptraining.views.screens.homescreen.HomeScreen
import com.binhdz.homeapptraining.views.screens.settingscreen.SettingScreen
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val mediaViewModel: MediaViewModel by viewModels()
    private val mediaInfoReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                MediaPlayerService.ACTION_SONG_INFO_UPDATED -> {
                    val musicItem =
                        intent.getParcelableExtra<MusicItem>(MediaPlayerService.EXTRA_MUSIC_ITEM)
                    if (musicItem != null) {
                        mediaViewModel.updateCurrentMusicItem(musicItem)
                        saveMusicItemToPreferences(this@MainActivity, musicItem)
                    }
                }

                MediaPlayerService.ACTION_VIDEO_INFO_UPDATED -> {
                    val videoItem =
                        intent.getParcelableExtra<VideoItem>(MediaPlayerService.EXTRA_VIDEO_ITEM)
                    if (videoItem != null) {
                        mediaViewModel.updateCurrentVideoItem(videoItem)
                      saveVideoItemToPreferences(this@MainActivity, videoItem)
                    }
                }
            }
        }
    }

    private fun registerReceivers() {
        val filter = IntentFilter().apply {
            addAction(MediaPlayerService.ACTION_SONG_INFO_UPDATED)
            addAction(MediaPlayerService.ACTION_VIDEO_INFO_UPDATED)
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mediaInfoReceiver, filter)
    }

    private fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mediaInfoReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerReceivers()
    }

    override fun onDestroy() {
        unregisterReceivers()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val musicItem = getMusicItemFromPreferences(this@MainActivity)
        musicItem?.let { mediaViewModel.updateCurrentMusicItem(it) }
        setContent {
            checkPermissions()
        }
    }

    private fun checkPermissions() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report?.areAllPermissionsGranted() == true) {
                        setContent {
                            TrainingApp()
                        }
                    } else {
                        Toast.makeText(this@MainActivity, "Permission denied!", Toast.LENGTH_LONG)
                            .show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .check()
    }

    @Composable
    fun TrainingApp() {
        val navController = rememberNavController()
        NavHost(navController = navController, startDestination = "home") {
            composable(
                "home",
                enterTransition = { enterTransition() },
                exitTransition = { exitTransition() }
            ) {
                HomeScreen(navController, mediaViewModel)
            }
            composable("setting") {
                SettingScreen(navController, mediaViewModel)
            }
        }
    }
}