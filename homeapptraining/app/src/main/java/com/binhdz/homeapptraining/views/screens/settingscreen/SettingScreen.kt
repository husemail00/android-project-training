package com.binhdz.homeapptraining.views.screens.settingscreen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.VerticalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.selection.selectable
import androidx.compose.material3.Divider
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavController
import com.binhdz.homeapptraining.viewmodels.MediaViewModel
import com.binhdz.homeapptraining.views.appbar.BlackSettingAppBar
import com.binhdz.homeapptraining.views.screens.settingscreen.settingoptions.AdjustVolume
import com.binhdz.homeapptraining.views.screens.settingscreen.settingoptions.ChoosingLanguage
import kotlinx.coroutines.launch

@Composable
fun SettingScreen(navController: NavController, mediaViewModel: MediaViewModel) {
    Scaffold(
        topBar = {
            BlackSettingAppBar(navController)
        },
        content = { innerPadding ->
            SettingScreenContent(innerPadding, mediaViewModel)
        }
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SettingScreenContent(innerPadding: PaddingValues, musicViewModel: MediaViewModel) {
    val totalPages = 2
    val pagerState = rememberPagerState(pageCount = {
        totalPages
    })
    ConstraintLayout(
        modifier = Modifier
            .padding(innerPadding)
            .fillMaxSize()
    ) {
        val (listSettings, settingViewPage) = createRefs()
        SettingIndicator(
            modifier = Modifier
                .fillMaxHeight()
                .width(150.dp)
                .border(
                    width = 1.dp,
                    color = Color.Blue,
                )
                .constrainAs(listSettings) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                },
            pagerState
        )
        SettingVerticalPager(
            modifier = Modifier
                .constrainAs(settingViewPage) {
                    top.linkTo(parent.top)
                    start.linkTo(listSettings.end)
                },
            pagerState,
            musicViewModel
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SettingIndicator(modifier: Modifier, pagerState: PagerState) {
    val indicationCoroutineScope = rememberCoroutineScope()
    Column(
        modifier = modifier
    ) {
        SettingButton("Language", pagerState.currentPage == 0) {
            indicationCoroutineScope.launch {
                pagerState.animateScrollToPage(0)
            }
        }
        Divider(color = Color.Blue, thickness = 1.dp)
        SettingButton("Volume", pagerState.currentPage == 1) {
            indicationCoroutineScope.launch {
                pagerState.animateScrollToPage(1)
            }
        }
        Divider(color = Color.Blue, thickness = 1.dp)
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SettingVerticalPager(modifier: Modifier, pagerState: PagerState, musicViewModel: MediaViewModel) {
    VerticalPager(modifier = modifier, state = pagerState) { page ->
        when (page) {
            0 -> ChoosingLanguage(musicViewModel)
            1 -> AdjustVolume(musicViewModel)
        }
    }
}


@Composable
fun SettingButton(
    buttonText: String,
    isSelected: Boolean,
    onButtonClick: () -> Unit
) {
    val textColor = if (isSelected) Color.White else Color.Black
    val buttonColor = if (isSelected) Color.Blue else Color.Transparent
    TextButton(
        onClick = onButtonClick,
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .background(buttonColor)
            .padding(8.dp)
            .selectable(
                selected = isSelected,
                onClick = {
                    onButtonClick()
                }
            )
    ) {
        Text(
            text = buttonText,
            color = textColor,
            fontWeight = if (isSelected) FontWeight.Bold else FontWeight.Normal
        )
    }
}