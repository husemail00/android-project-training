package com.binhdz.homeapptraining.views.screens.homescreen

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.binhdz.homeapptraining.R
import com.binhdz.homeapptraining.utils.getEmbeddedVideoThumbnail
import com.binhdz.homeapptraining.viewmodels.MediaViewModel

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ItemListHomeVideo(mediaViewModel: MediaViewModel, onClick: () -> Unit) {
    val context = LocalContext.current
    val currentVideoItem by mediaViewModel.currentVideoItem.collectAsState()
    var thumbnail by remember { mutableStateOf<Bitmap?>(null) }
    LaunchedEffect(currentVideoItem.path) {
        thumbnail = getEmbeddedVideoThumbnail(context, currentVideoItem.path)
    }


    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp
    val widthItemHome = (screenHeight - 70) * 0.9
    val heightItemHome = (screenHeight - 70) * 0.8
    Box(
        modifier = Modifier
            .width(widthItemHome.dp)
            .height(heightItemHome.dp)
            .border(
                width = 1.dp,
                color = Color.Blue,
                shape = RoundedCornerShape(30.dp)
            )
            .background(
                Color.Gray.copy(alpha = 0.9f),
                shape = RoundedCornerShape(30.dp)
            )
            .clickable {
                onClick()
            },
        contentAlignment = Alignment.Center
    ) {
        val maxItemSize = (screenHeight * 0.4)
        Column(
            modifier = Modifier.width(maxItemSize.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Video",
                color = Color.White,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = TextStyle(
                    color = Color.White,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            )
            Spacer(modifier = Modifier.height(8.dp))
            thumbnail?.let {
                Image(
                    bitmap = it.asImageBitmap(),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .clip(CircleShape)
                        .size(maxItemSize.dp)
                )
            } ?: Image(
                bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_videoplayback)
                    .asImageBitmap(),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .clip(CircleShape)
                    .size(maxItemSize.dp)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = currentVideoItem.title,
                color = Color.White,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = TextStyle(
                    color = Color.White,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier
                    .padding(bottom = 14.dp)
                    .basicMarquee(
                        Int.MAX_VALUE,
                        animationMode = MarqueeAnimationMode.Immediately,
                        delayMillis = 1000
                    )
            )
        }
    }
}