package com.binhdz.homeapptraining.views.screens.homescreen
import androidx.compose.animation.Animatable
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import com.binhdz.homeapptraining.utils.getEmbeddedAlbumArt
import com.binhdz.homeapptraining.viewmodels.MediaViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ItemListHomeMusic(mediaViewModel: MediaViewModel, onClick: () -> Unit) {
    val context = LocalContext.current
    val currentMusicItem by mediaViewModel.currentMusicItem.collectAsState()
    val currentSongImage by rememberUpdatedState(
        getEmbeddedAlbumArt(
            currentMusicItem.path,
            context
        )
    )
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp
    val widthItemHome = (screenHeight - 70) * 0.9
    val heightItemHome = (screenHeight - 70) * 0.8
    Box(
        modifier = Modifier
            .width(widthItemHome.dp)
            .height(heightItemHome.dp)
            .border(
                width = 1.dp,
                color = Color.Blue,
                shape = RoundedCornerShape(30.dp)
            )
            .background(
                Color.Gray.copy(alpha = 0.9f),
                shape = RoundedCornerShape(30.dp)
            )
            .clickable {
                onClick()
            },
        contentAlignment = Alignment.Center
    ) {
        val maxItemSize = (screenHeight * 0.4)
        Column(
            modifier = Modifier.width(maxItemSize.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Music",
                color = Color.White,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = TextStyle(
                    color = Color.White,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            )
            Spacer(modifier = Modifier.height(8.dp))
            AnimatedBorderImage(currentSongImage.asImageBitmap(), maxItemSize)
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = currentMusicItem.title,
                color = Color.White,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = TextStyle(
                    color = Color.White,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.basicMarquee(
                    Int.MAX_VALUE,
                    animationMode = MarqueeAnimationMode.Immediately,
                    delayMillis = 1000
                )
            )
            Text(
                text = currentMusicItem.artist,
                color = Color.White,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = TextStyle(
                    color = Color.White,
                    fontSize = 14.sp,
                )
            )
        }
    }
}

@Composable
fun AnimatedBorderImage(currentSongImage: ImageBitmap, maxItemSize: Double) {
    val animatedColor = remember { Animatable(Color.DarkGray) }
    val animatedBorderModifier = Modifier.drawWithContent {
        drawContent()
        drawCircle(
            color = animatedColor.value,
            radius = size.minDimension / 2,
            style = Stroke(width = 6.dp.toPx())
        )
    }
    val infiniteTransition = rememberInfiniteTransition(label = "")
    val angle by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 360f,
        animationSpec = infiniteRepeatable(
            animation = keyframes {
                durationMillis = Random.nextInt(10000, 15001)
            }
        ), label = ""
    )
    LaunchedEffect(key1 = animatedColor) {
        launch {
            while (true) {
                val randomColor = Color(
                    red = (0..255).random() / 255f,
                    green = (0..255).random() / 255f,
                    blue = (0..255).random() / 255f,
                    alpha = 1f
                )
                animatedColor.animateTo(
                    targetValue = randomColor,
                    animationSpec = tween(durationMillis = 1000)
                )
                delay(1000)
            }
        }
    }
    Image(
        bitmap = currentSongImage,
        contentDescription = "",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .rotate(angle)
            .clip(CircleShape)
            .size(maxItemSize.dp)
            .then(animatedBorderModifier)
    )
}
