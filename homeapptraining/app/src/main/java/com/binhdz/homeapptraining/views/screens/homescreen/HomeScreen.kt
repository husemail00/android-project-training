package com.binhdz.homeapptraining.views.screens.homescreen
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.core.app.TaskStackBuilder
import androidx.navigation.NavController
import com.binhdz.homeapptraining.R
import com.binhdz.homeapptraining.viewmodels.MediaViewModel
import com.binhdz.homeapptraining.views.appbar.BlackHomeAppBar
import kotlinx.coroutines.launch
@Composable
fun HomeScreen(navController: NavController, mediaViewModel: MediaViewModel) {
    Scaffold(
        topBar = {
            BlackHomeAppBar()
        },
        content = { innerPadding ->
            HomeScreenContent(innerPadding, navController, mediaViewModel)
        }
    )
}
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeScreenContent(
    innerPadding: PaddingValues,
    navController: NavController,
    mediaViewModel: MediaViewModel
) {
    val totalPages = 2
    val pagerState = rememberPagerState(pageCount = {
        totalPages
    })
    ConstraintLayout {
        val (pager, indicator) = createRefs()
        HomeHorizontalPager(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues = innerPadding)
                .constrainAs(pager) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom, margin = 16.dp)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
            pagerState,
            navController,
            mediaViewModel
        )
        HomeIndicator(
            Modifier
                .fillMaxWidth()
                .constrainAs(indicator) {
                    bottom.linkTo(parent.bottom, margin = 8.dp)
                }, pagerState
        )
    }
}
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeHorizontalPager(
    modifier: Modifier,
    pagerState: PagerState,
    navController: NavController,
    mediaViewModel: MediaViewModel
) {
    val context: Context = LocalContext.current
    HorizontalPager(
        state = pagerState,
        modifier = modifier
    ) { page ->
        when (page) {
            0 -> Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                ItemListHomeMusic(mediaViewModel = mediaViewModel) {
                    navigateToMediaPlayerApp(context, mediaViewModel.currentMusicItem.value.id)
                }
                ItemListHomeVideo(mediaViewModel = mediaViewModel) {
                    navigateToVideoPlayerApp(context)
                }
            }
            1 -> Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                ItemListHomeSetting {
                    navController.navigate("setting")
                }
                ItemListHomeEmpty()
            }
        }
    }
}
fun navigateToMediaPlayerApp(context: Context, currentSongID: Int) {
    val intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("mediaplayerapptraining://com.binhdz.mediaplayerapptraining.musicplayback/${currentSongID}")
    )
    val pendingIntent = TaskStackBuilder.create(context).run {
        addNextIntentWithParentStack(intent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
            )
        } else {
            getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        }
    }
    if (pendingIntent != null) {
        pendingIntent.send()
    }
}
fun navigateToVideoPlayerApp(context: Context) {
    val intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("mediaplayerapptraining://com.binhdz.mediaplayerapptraining.videoplayback")
    )
    val pendingIntent = TaskStackBuilder.create(context).run {
        addNextIntentWithParentStack(intent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
            )
        } else {
            getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        }
    }
    if (pendingIntent != null) {
        pendingIntent.send()
    }
}
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeIndicator(modifier: Modifier, pagerState: PagerState) {
    val indicationCoroutineScope = rememberCoroutineScope()
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.Center
    ) {
        repeat(pagerState.pageCount) { iteration ->
            val color = if (pagerState.currentPage == iteration) Color.Blue else Color.LightGray
            Box(
                modifier = Modifier
                    .padding(4.dp)
                    .clip(RoundedCornerShape(2.dp))
                    .background(color)
                    .height(7.dp)
                    .width(70.dp)
                    .clickable {
                        indicationCoroutineScope.launch {
                            pagerState.animateScrollToPage(iteration)
                        }
                    }
            )
        }
    }
}