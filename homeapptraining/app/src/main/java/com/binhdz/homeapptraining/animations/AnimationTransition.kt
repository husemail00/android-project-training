package com.binhdz.homeapptraining.animations

import androidx.compose.animation.*
import androidx.compose.animation.core.*

fun enterTransition(): EnterTransition = slideInHorizontally(
    initialOffsetX = { 1000 },
    animationSpec = spring(
        dampingRatio = Spring.DampingRatioMediumBouncy,
        stiffness = Spring.StiffnessLow
    )
) + fadeIn(animationSpec = tween(700, easing = FastOutSlowInEasing))

fun exitTransition(): ExitTransition = fadeOut(
    animationSpec = tween(
        durationMillis = 300,
        easing = LinearOutSlowInEasing
    )
) + slideOutHorizontally(
    targetOffsetX = { -it },
    animationSpec = tween(
        durationMillis = 300,
        easing = FastOutLinearInEasing
    )
)