package com.binhdz.homeapptraining.utils

import android.os.Environment
import com.binhdz.homeapptraining.models.AppSettings
import com.google.gson.Gson
import java.io.File
import java.io.FileWriter
import java.io.IOException

class JsonHelper {
    companion object {
        private val SETTING_FILE_PATH: String =
            "${File(Environment.getExternalStorageDirectory().absolutePath + "/Download")}/setting.txt"

        fun saveAppSettings(appSettings: AppSettings = AppSettings(0, 5)): AppSettings {
            val jsonContent = """
{
"language": ${appSettings.language},
"volume": ${appSettings.volume}
}
""".trimIndent()
            writeFile(SETTING_FILE_PATH, jsonContent)
            return appSettings
        }

        fun getAppSettings(): AppSettings? {
            val jsonString = readFile(SETTING_FILE_PATH)
            return try {
                val gson = Gson()
                gson.fromJson(jsonString, AppSettings::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        private fun writeFile(filePath: String, newContent: String) {
            try {
                val file = File(filePath)
                val fileWriter = FileWriter(file)
                fileWriter.write(newContent)
                fileWriter.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        private fun readFile(filePath: String): String {
            val file = File(filePath)
            return if (file.exists()) {
                file.readText()
            } else {
                ""
            }
        }
    }
}