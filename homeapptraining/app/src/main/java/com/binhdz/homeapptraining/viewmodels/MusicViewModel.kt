package com.binhdz.homeapptraining.viewmodels
import androidx.lifecycle.ViewModel
import com.binhdz.homeapptraining.models.AppSettings
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.homeapptraining.utils.JsonHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
class MediaViewModel : ViewModel() {
    // setting
    private val _appSettingsState = MutableStateFlow(AppSettings())
    val appSettingsState: StateFlow<AppSettings> = _appSettingsState
    private fun loadAppSettings() {
// Lấy AppSettings từ file JSON
        val settings = JsonHelper.getAppSettings() ?: AppSettings()
        _appSettingsState.value = settings
    }
    fun updateAppSettings(newSettings: AppSettings) {
        _appSettingsState.value = newSettings
        JsonHelper.saveAppSettings(newSettings)
    }
    // music
    private val _currentMusicItemState = MutableStateFlow(
        MusicItem(
            -1, "Chose a song to play.", "tap here!", "undefined", 0, "unknown path"
        )
    )
    val currentMusicItem: StateFlow<MusicItem> = _currentMusicItemState
    fun updateCurrentMusicItem(newMusicItem: MusicItem) {
        _currentMusicItemState.value = newMusicItem
    }
    // video
    private val _currentVideoItemState = MutableStateFlow(
        VideoItem(
            -1, "Chose a video to play.", "tap here!", "unknown path", 0
        )
    )
    val currentVideoItem: StateFlow<VideoItem> = _currentVideoItemState
    fun updateCurrentVideoItem(newVideoItem: VideoItem) {
        _currentVideoItemState.value = newVideoItem
    }
    init {
        loadAppSettings()
    }
}