package com.binhdz.homeapptraining.services
import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.binhdz.homeapptraining.IMediaPlayerService
import com.binhdz.homeapptraining.models.AppSettings
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.binhdz.homeapptraining.utils.JsonHelper
class MediaPlayerService : Service() {
    companion object {
        const val ACTION_SONG_INFO_UPDATED = "action_song_info_updated"
        const val EXTRA_MUSIC_ITEM = "extra_music_item"
        const val ACTION_VIDEO_INFO_UPDATED = "action_video_info_updated"
        const val EXTRA_VIDEO_ITEM = "extra_video_item"
    }
    private fun notifySongInfoUpdated(musicItem: MusicItem?) {
        val intent = Intent(ACTION_SONG_INFO_UPDATED)
        intent.putExtra(EXTRA_MUSIC_ITEM, musicItem)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
    private fun notifyVideoInfoUpdated(videoItem: VideoItem?) {
        val intent = Intent(ACTION_VIDEO_INFO_UPDATED)
        intent.putExtra(EXTRA_VIDEO_ITEM, videoItem)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
    override fun onBind(intent: Intent?): IBinder {
        return binder
    }
    private val binder: IMediaPlayerService.Stub = object : IMediaPlayerService.Stub() {
        override fun getCurrentAppSetting(): AppSettings {
            return JsonHelper.getAppSettings() ?: AppSettings(0, 5)
        }
        override fun saveCurrentSongInfo(musicItem: MusicItem?) {
            notifySongInfoUpdated(musicItem)
        }
        override fun saveCurrentVideoInfo(videoItem: VideoItem?) {
            notifyVideoInfoUpdated(videoItem)
        }
    }
}