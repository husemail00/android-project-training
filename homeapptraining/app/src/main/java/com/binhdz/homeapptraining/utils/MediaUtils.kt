package com.binhdz.homeapptraining.utils
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.net.Uri
import com.binhdz.homeapptraining.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
// get song image
fun getEmbeddedAlbumArt(filePath: String, context: Context): Bitmap {
    val file = File(filePath)
    if (file.exists() && file.isFile) {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(filePath)
        val picture = retriever.embeddedPicture
        retriever.release()
        if (picture != null) {
            val bitmapImage = BitmapFactory.decodeByteArray(picture, 0, picture.size)
            if (bitmapImage != null)
                return bitmapImage
        }
    }
    return BitmapFactory.decodeResource(context.resources, R.drawable.ic_music)
}

suspend fun getEmbeddedVideoThumbnail(context: Context, videoPath: String): Bitmap? {
    return withContext(Dispatchers.IO) {
        val retriever = MediaMetadataRetriever()
        try {
            retriever.setDataSource(context, Uri.parse(videoPath))
            retriever.getFrameAtTime()
        } catch (e: Exception) {
            null
        } finally {
            retriever.release()
        }
    }
}