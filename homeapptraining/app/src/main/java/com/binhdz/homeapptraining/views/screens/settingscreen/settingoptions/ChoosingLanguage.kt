package com.binhdz.homeapptraining.views.screens.settingscreen.settingoptions

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.selection.selectable
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.binhdz.homeapptraining.models.Languages
import com.binhdz.homeapptraining.viewmodels.MediaViewModel

@Composable
fun ChoosingLanguage(mediaViewModel: MediaViewModel) {
    RadioButtonGroup(mediaViewModel)
}

@Composable
fun RadioButtonGroup(mediaViewModel: MediaViewModel) {
    val appSettings by mediaViewModel.appSettingsState.collectAsState()
    var selectedOption by remember {
        mutableStateOf(if (appSettings.language == 0) Languages.English else Languages.Vietname)
    }

    fun saveSelection(option: Languages) {
        val appLanguage = if (option == Languages.English) 0 else 1
        val updatedSettings = appSettings.copy(language = appLanguage)
        mediaViewModel.updateAppSettings(updatedSettings)
        selectedOption = option
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Languages.entries.forEach { option ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
                    .padding(8.dp)
                    .selectable(
                        selected = option == selectedOption,
                        onClick = {
                            saveSelection(option)
                        }
                    )
            ) {
                RadioButton(
                    selected = option == selectedOption,
                    onClick = {
                        saveSelection(option)
                    },
                    colors = RadioButtonDefaults.colors(
                        unselectedColor = Color.Black,
                        selectedColor = Color.Black,
                    )
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = when (option) {
                        Languages.English -> "English"
                        Languages.Vietname -> "Vietnam"
                    },
                    color = Color.Black,
                    modifier = Modifier.weight(1f),
                    fontWeight = if (option == selectedOption) FontWeight.Bold else FontWeight.Normal
                )
            }
        }
    }
}