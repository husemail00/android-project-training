package com.binhdz.homeapptraining.views.screens.homescreen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp

@Composable
fun ItemListHomeEmpty() {
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp

    val widthItemHome = (screenHeight - 70) * 0.9
    val heightItemHome = (screenHeight - 70) * 0.8

    Box(
        modifier = Modifier
            .width(widthItemHome.dp)
            .height(heightItemHome.dp)
    )
}