package com.binhdz.homeapptraining.utils
import android.content.Context
import com.binhdz.homeapptraining.models.AppSettings
import com.binhdz.homeapptraining.models.MusicItem
import com.binhdz.homeapptraining.models.VideoItem
import com.google.gson.Gson
fun saveMusicItemToPreferences(context: Context, musicItem: MusicItem) {
    val sharedPreferences = context.getSharedPreferences("MusicPreferences", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    val gson = Gson()
    val json = gson.toJson(musicItem)
    editor.putString("LastMusicItem", json)
    editor.apply()
}
fun getMusicItemFromPreferences(context: Context): MusicItem? {
    val sharedPreferences = context.getSharedPreferences("MusicPreferences", Context.MODE_PRIVATE)
    val gson = Gson()
    val json = sharedPreferences.getString("LastMusicItem", null)
    return if (json != null) gson.fromJson(json, MusicItem::class.java) else null
}
fun saveVideoItemToPreferences(context: Context, videoItem: VideoItem) {
    val sharedPreferences = context.getSharedPreferences("MusicPreferences", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    val gson = Gson()
    val json = gson.toJson(videoItem)
    editor.putString("LastVideoItem", json)
    editor.apply()
}
fun getVideoItemFromPreferences(context: Context): VideoItem? {
    val sharedPreferences = context.getSharedPreferences("MusicPreferences", Context.MODE_PRIVATE)
    val gson = Gson()
    val json = sharedPreferences.getString("LastVideoItem", null)
    return if (json != null) gson.fromJson(json, VideoItem::class.java) else null
}