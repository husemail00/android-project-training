package com.drivemodeteam.drivemodeproject

import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicText
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
//            BaseScreen()
//            CustomSelectableButton()
            RowButton()
//            PreviewImageWithCenteredText()

        }
    }
}


@Composable
fun PreviewImageWithCenteredText() {
    val imagePainter = painterResource(id = R.drawable.sample_image) // Thay 'your_image_resource' bằng tên tài nguyên hình ảnh của bạn
    ImageWithCenteredText(imagePainter = imagePainter, text = "Văn bản ở giữa")
}

@Composable
fun ImageWithCenteredText(imagePainter: Painter, text: String) {
    Box(modifier = Modifier.size(376.dp)) {
        Image(
            painter = imagePainter,
            contentDescription = null,
            modifier = Modifier.fillMaxSize().background(Color.Yellow)
        )
        Text(
            text = text,
            color = Color.White, // Đặt màu văn bản, thay đổi nếu cần
            modifier = Modifier.align(Alignment.Center), // Đặt văn bản ở giữa Box
            textAlign = TextAlign.Center, // Căn giữa văn bản
            style = TextStyle(background = Color.Black.copy(alpha = 0.5f)) // Tùy chỉnh style văn bản
        )
    }
}


@Preview(device = "spec:shape=Normal,width=1688,height=1404,unit=dp,dpi=160")
@Composable
fun RowButton() {
    var selectedButton by remember { mutableStateOf(1) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(10.dp))
            .background(Color.Gray)
    ) {
        (1..4).forEach { index ->
            Box(
                modifier = Modifier
                    .weight(1f)
                    .clip(RoundedCornerShape(10.dp))
                    .background(if (selectedButton == index) Color.Blue else Color.Transparent)
            ) {
                TextButton(
                    onClick = { selectedButton = index },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(text = "Button$index", color = Color.White)
                }
            }
        }
    }
}


@Composable
fun RowButton2() {
    var selectedButton by remember { mutableStateOf(1) }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(10.dp))
            .background(Color.Gray)
    ) {
        (1..4).forEach { index ->
            // Sử dụng animateColorAsState để tạo hiệu ứng chuyển màu mượt mà
            val backgroundColor by animateColorAsState(
                targetValue = if (selectedButton == index) Color.Blue else Color.Transparent
            )

            Box(
                modifier = Modifier
                    .weight(1f)
                    .clip(RoundedCornerShape(10.dp))
                    .background(backgroundColor) // Áp dụng màu nền đã được animate
            ) {
                TextButton(
                    onClick = { selectedButton = index },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(text = "Button$index", color = Color.White)
                }
            }
        }
    }
}

@Composable
fun BaseScreen() {
    Column(modifier = Modifier.fillMaxSize()) {
        /////// top
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)
                .background(Color.Gray)
        ) {
            Text(text = "Khu vuc indicator")
        }

        /////// middle
        Row(
            modifier = Modifier
                .weight(1f)
                .height(0.dp)
        ) {
            Column(modifier = Modifier.fillMaxHeight()) {
                Box( // thay thế bằng danh sách các nút đặt dọc
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(200.dp)
                        .background(Color.Blue)
                )
            }

            Box( // thay thế bằng danh sách các nút đặt dọc
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Yellow)
            )
        }


        /////// bottom
        Box( // row các buttons có space evenly
            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)
                .background(Color.Green)
        ) {
            Text(text = "Khu vuc row buttons")
        }
    }
}


@Composable
fun CustomSelectableButton() {
    var isSelected by remember { mutableStateOf(false) }
    Box(
        modifier = Modifier
            .width(120.dp)
            .clickable { isSelected = !isSelected }
            .padding(16.dp),
        contentAlignment = Alignment.Center
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = "Button",
                fontSize = 16.sp,
                color = if (isSelected) Color.Blue else Color.Black,
                modifier = Modifier.padding(8.dp)
            )
            if (isSelected) {
                Divider(
                    color = Color.Blue,
                    thickness = 2.dp,
                    modifier = Modifier
                        .background(color = Color.Blue, shape = RoundedCornerShape(4.dp))
                        .height(2.dp)
//                        .width(120.dp)
                )
            }
        }
    }
}


